import Control.Monad

bits n
  | n == 0 = 0
  | n >= 4096 = (quot n 2048) + (bits (n `mod` 2048))
  | n `mod` 2 == 0 = tmp
  | otherwise = 1 + tmp
  where tmp = bits$ quot n 2

main = do
  t <- readLn
  forM [1..t] (\i -> do
    n <- readLn
    print$ bits n)
