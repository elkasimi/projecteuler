import Control.Monad

z n
  | n < 5 = 0
  | otherwise = p + (z p)
  where p = (quot n 5)

main = do
  t <- readLn
  forM [1..t] (\i -> do
    n <- readLn
    print (z n))
