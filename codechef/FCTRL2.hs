import Control.Monad

factorial 0 = 1
factorial n = n * (factorial (n - 1))

main = do
  t <- readLn
  forM [1..t] (\i -> do
    n <- readLn
    print (factorial n))
