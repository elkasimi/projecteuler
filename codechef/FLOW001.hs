process t = do
  line <- getLine
  let splits = words line
      a = read (splits!!0) ::Int
      b = read (splits!!1) ::Int
  print$ a+b
  if t /= 1
    then do process (t-1)
  else
    return ()

main = do
  t <- readLn
  process t
