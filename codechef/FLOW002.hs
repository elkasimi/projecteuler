import Control.Monad

main :: IO ()
main = do
  t <- readLn
  forM_ [1..t] (\i -> do
    line <- getLine
    let l = words line
        readInt = read :: String -> Int
        a = (readInt . head) l
        b = (readInt . last) l
    print $ mod a b)
