import Data.Char

process t = do
  n <- readLn ::IO Int
  print$ sum (map digitToInt (show n))
  if t /= 1
    then do process (t-1)
  else
    return ()

main = do
  t <- readLn
  process t
