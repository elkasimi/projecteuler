import qualified Text.Printf as Format

solve :: Int -> Float -> Float
solve x y
  | x `mod` 5 == 0 && fx + 0.5 < y = y - fx - 0.5
  | otherwise = y
  where fx = (fromIntegral::Int->Float) x

main = do
  line <- getLine
  let result = solve x y
                 where parts = words line
                       x = (read::String->Int) (parts!!0)
                       y = (read::String->Float) (parts!!1)
  Format.printf "%.2f\n" result
