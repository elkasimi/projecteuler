process 0 k = return 0
process n k = do
  i <- readLn
  if i `mod` k == 0
    then do
      t <- process (n-1) k
      return (1 + t)
    else do
      process (n-1) k

main = do
  line <- getLine
  let parts = words line
      n = read$ parts!!0 
      k = read$ parts!!1
  result <- process n k
  print result
