import qualified Data.Set as Set

process :: [String] -> IO () 
process [] = return ()
process (firstLine : secondLine : nextLines) = do
  let parts = words firstLine
      k = read (parts!!1)
      set = Set.fromList $ words secondLine
  print $ hex set (k, 0)
  do process nextLines 

hex :: Set.Set String -> (Int, Int) -> Int
hex set (k, current)
  | (show current) `elem` set = hex set (k, current + 1)
  | otherwise = if k == 0 then current else hex set (k-1, current+1)

main :: IO()
main = do
  t <- readLn :: IO Int
  contents <- getContents
  let inputs = lines contents
  process $ inputs
