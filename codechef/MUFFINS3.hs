process t = do
  n <- readLn
  print$ 1 + (quot n 2)
  if t /= 1
    then do process (t-1)
  else
    return ()

main = do
  t <- readLn
  process t
