import Control.Monad

readInts = do
  line <- getLine
  return$ map read (words line) 

balancedContest stats participants
  | 1 /= count (>=half) stats = False
  | 2 /= count (<=tenth) stats = False
  | otherwise = True
    where half = div participants 2
          tenth = div participants 10
          count predicate = length.filter predicate

main = do
  t <- readLn
  forM [1..t] (\i -> do
    ints <- readInts
    let p = last ints
    stats <- readInts
    let balanced = balancedContest stats p
    putStrLn$ if balanced then "yes" else "no")
