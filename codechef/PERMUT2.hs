import Data.Array

readInt = read :: String -> Int

readInts :: IO [Int]
readInts = do
  line <- getLine
  return $ map readInt (words line)

process :: IO ()
process = do
  n <- readLn
  if n == 0 then return ()
  else do
    permutation <- readInts
    let a = array (1, n) (zip [1..] permutation)
    putStrLn $ if all (\i -> a ! (a ! i) == i) [1..n] then "ambiguous" else "not ambiguous"
    process

main :: IO ()
main = process
