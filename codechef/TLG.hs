process sum1 sum2 lead1 lead2 0
  | lead1 > lead2 = return$ "1 " ++ (show lead1)
  | otherwise = return$ "2 " ++ (show lead2)

process sum1 sum2 lead1 lead2 n = do
  line <- getLine
  let splits = words line
  let x = read$ splits!!0
  let y = read$ splits!!1
  let s1 = x + sum1
  let s2 = y + sum2
  let l1 = max lead1 (s1-s2)
  let l2 = max lead2 (s2-s1)
  result <- process s1 s2 l1 l2 (n-1)
  return result 

main = do
  n <- readLn
  result <- process 0 0 0 0 n
  putStrLn result
