import Control.Monad
import Data.List

main = do
  contents <- getContents
  let inputs = tail$ lines contents
  let numbers = map (read::String-> Int) inputs
  forM (sort numbers) print
