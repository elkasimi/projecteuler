import Data.Maybe (fromJust)
import Data.List (maximumBy, elemIndex)
import Data.Ord (comparing)

cycleLength :: Int -> [Int] -> Int -> Int
cycleLength n seq r
  | i /= Nothing = 1 + fromJust i
  | otherwise = cycleLength n (t:seq) t
    where t = mod (10 * r) n
          i = elemIndex t seq


main = do
	print $ maximumBy (comparing fst) [(cycleLength i [] 1, i) | i <- [1..999]]
