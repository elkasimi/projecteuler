import Data.List (maximumBy)
import Data.Ord (comparing)

isPrime :: Int -> Int -> Bool
isPrime n f
  | n < 2 = False
  | f*f > n = True
  | mod n f == 0 = False
  | otherwise = isPrime n (if f == 2 then 3 else f+2)

lengthPrimes :: (Int, Int, Int) -> Int
lengthPrimes (n, a, b) 
  | nprime = 1 + lengthPrimes (n+1, a, b)
  | otherwise = 0
    where nprime = isPrime (n*n + a*n + b) 2

a_range = [-999..999]
b_range = [-1000..1000]

main = do
  print $ maximumBy (comparing head) [[lengthPrimes (0, a, b), a, b] | a <- a_range, b <- b_range]
