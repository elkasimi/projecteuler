import Data.Char (digitToInt)

sumFifthPowers :: Int -> Int
sumFifthPowers n = sum [(digitToInt d) ^ 5 | d <- (show n)]

main :: IO ()
main = print $ sum [i | i <- [2..999999], sumFifthPowers i == i]
