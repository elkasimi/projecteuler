import Data.Array


next :: Int -> Int
next n = case n of 200 -> 100
                   100 -> 50
                   50 -> 20
                   20 -> 10
                   10 -> 5
                   5 -> 2
                   2 -> 1

coins = [1, 2, 5, 10, 20, 50, 100, 200]

ways :: Int -> Array (Int, Int) Int
ways amount = result
     where result = array ((1, 1), (amount, amount))
                      ( [((a, 1), 1) | a <- [0..amount]] ++
                        [((0, c), 1) | c <- coins] ++
                        [((a, c), sum [result ! (a-i, (next c)) | i <- [c, 2*c..a]])
                        | a <- [0..amount], c <- tail coins])

main :: IO ()
main = do
  -- print$ (ways 200) ! (200, 200)
  print $ bounds (ways 200)
