sumMultiples n = quot (n * c * (c + 1)) 2
  where c = quot 999 n
main = do
  print $ (sumMultiples 3) + (sumMultiples 5) - (sumMultiples 15)
