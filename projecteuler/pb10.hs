import Data.Set

limit = 2000000

removeMultiple numbers a i
  | a > limit = numbers
  | otherwise = removeMultiple (delete a numbers) (a+i) i

sieve numbers a
  | a*a > limit = numbers
  | a `notMember` numbers = sieve numbers (a + 1)
  | otherwise = sieve (removeMultiple numbers (a*a) a) (a+1)

main = do
  print $ sum(sieve (fromList [2..limit]) 2)
