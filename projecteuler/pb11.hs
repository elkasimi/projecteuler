import Control.Monad

lineProduct matrix a b n
  | b == 20 = 0
  | n == 0 = 1
  | otherwise = (lineProduct matrix a (b+1) (n-1)) * (matrix!!a)!!b

columnProduct matrix a b n
  | a == 20 = 0
  | n == 0 = 1
  | otherwise = (columnProduct matrix (a+1) b (n-1)) * (matrix!!a)!!b

firstDiagonal matrix a b n
  | a == 20 || b == 20 = 0
  | n == 0 = 1
  | otherwise = (firstDiagonal matrix (a+1) (b+1) (n-1)) * (matrix!!a)!!b

secondDiagonal matrix a b n
  | a == -1 || b == 20 = 0
  | n == 0 = 1
  | otherwise = (secondDiagonal matrix (a-1) (b+1) (n-1)) * (matrix!!a)!!b

cellProduct matrix a b = max (max l c) (max d1 d2)
  where l = lineProduct matrix a b 4
        c = columnProduct matrix a b 4
        d1 = firstDiagonal matrix a b 4
        d2 = secondDiagonal matrix a b 4

maxProduct matrix = result
  where result = maximum [cellProduct matrix a b | a <- [0..19], b <- [0..19]] 

main = do
  matrix <- forM[1..20] (\a -> do
    line <- getLine
    return (map (read::String->Int) (words line)))

  print (maxProduct matrix)
