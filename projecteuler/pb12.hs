divisorCount n i sum
  | i * i > n = sum
  | n `mod` i == 0 = divisorCount n (i + 1) (sum + c)
  | otherwise = divisorCount n (i + 1) sum
  where c
          | i * i == n = 1
          | otherwise = 2

findTriangleNumber n
  | (divisorCount t 1 0) > 5 = t
  | otherwise = findTriangleNumber (n+1)
  where t = quot (n*n + n) 2

main = do
  print $ findTriangleNumber 1
