import Control.Monad

main = do
  numbers <- forM [1..100] (\i -> do
    line <- getLine
    return ((read::String->Integer) line))

  print$ (take 10 (show$ sum numbers))
