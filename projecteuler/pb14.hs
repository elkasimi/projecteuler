import qualified Data.Map as Map

next n
  | even n = quot n 2
  | otherwise = 3 * n + 1

lengthCollatzSequence n cache
  | n == 1 = 1
  | n `Map.member` cache = cache Map.! n
  | otherwise = 1 + (lengthCollatzSequence (next n) cache)

findMaxLength (number, length, cache) current
  | current >= limit = (number, length)
  | otherwise = findMaxLength result (current + 1)
  where result
          | l <= length = (number, length, (Map.insert current l cache))
          | otherwise = (current, l, (Map.insert current l cache))
          where l = lengthCollatzSequence current cache
        limit = 1000000

main = do
  print $ findMaxLength (1, 1, Map.empty) 2
