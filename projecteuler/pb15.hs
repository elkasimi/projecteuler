
routes n = quot (factorial (2 * n)) ((factorial n) ^ 2)
  where factorial n
          | n == 0 = 1
          | otherwise = n * (factorial (n-1))

main = do
  n <- readLn
  print $ routes n
