import Data.Char

s = show (2^1000)
main = do
  print $ sum [digitToInt c | c <- s]
