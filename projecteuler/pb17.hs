import Control.Monad

maxTotal triangle last
  | (length last) == 15 = maximum last
  | otherwise = maxTotal triangle row
  where x = triangle!!(length last)
        at index seq
          | index < 0 = 0
          | index >= (length seq) = 0
          | otherwise = seq !! index
        row = [x!!i + (max (at i last) (at (i-1) last) ) | i <- [0..(length last)]]

main = do
  triangle <- forM [1..15] (\a -> do
    line <- getLine
    return (map (read::String->Int) (words line)))
  print$ maxTotal triangle (triangle!!0)
