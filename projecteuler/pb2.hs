fib a b c
  | a < limit = fib (4*a+b) a (c+a)
  | otherwise = c
  where limit = 4000000

main = do
  print $ fib 2 0 0
