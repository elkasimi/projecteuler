import qualified Data.Set as Set

sumDivisors n i
  | ixi > n = 0
  | n `mod` i /= 0 = t
  | ixi == n = i + t
  | otherwise = i + (div n i) + t
  where ixi = i*i
        t = sumDivisors n (i+1)

limit = 28123
abundants = filter (\i -> 2 * i < sumDivisors i 1) [1..limit]

main = do
  let set = Set.fromList [a + b | a <- abundants, b <- abundants, a <= b && a + b <= limit]
  print $ (sum [1..limit]) - (sum set)
