solve :: (Integer, Integer, Int) -> Int
solve (a, b, i)
  | length (show a) == 1000 = i
  | otherwise = solve (a+b, a, i+1)

main :: IO ()
main = print $ solve (1, 1, 2)
