findLargestPrimeFactor n f factor
  | n < f * f = max factor n
  | n `mod` f == 0 = findLargestPrimeFactor m f (max factor f)
  | otherwise = findLargestPrimeFactor n (f + 1) factor
  where m = quot n f

main = do
  print $ findLargestPrimeFactor 600851475143 2 1
