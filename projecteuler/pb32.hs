import Data.List (permutations)
import Data.Set (fromList, toList)

number :: String -> Int -> Int -> Int
number permutation a b = (read :: String -> Int) [permutation !! i | i <- [a..b]]

pandigits :: String -> [Int] 
pandigits permutation = [c | i <- [0..8], j <- [i+1..8], k <- [j+1..8],
                             8-k < j,
                             let a = number permutation 0 i
                                 b = number permutation (i+1) j
                                 c = number permutation (j+1) 8,
                              a * b == c]

main :: IO ()
main = do
  let s = fromList $ concat [pandigits permutation | permutation <- permutations "123456789"]
  print $ sum (toList s)
