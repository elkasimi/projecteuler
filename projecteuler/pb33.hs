main = print $ [let d = (gcd y z); y'=(div y d); z'=(div z d) in if y < z then (y', z') else (z', y') |
               x <- [1..9],
               y <- [1..9],
               z <- [1..9],
               let a = 10*x+y
                   b = 10*z + x,
               a * z == b * y && x /= y && x /= z]
