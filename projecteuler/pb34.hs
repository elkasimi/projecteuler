import Data.Char (digitToInt)
import Data.Array

factorial = array (0, 9) ([(0, 1)] ++ [(i, i * (factorial ! (i-1))) | i <- [1..9]])

sumFactorialDigits n = n == sum [factorial ! (digitToInt c) | c <- show n]

main = print $ sum [i | i <- [3..10000000], sumFactorialDigits i]
