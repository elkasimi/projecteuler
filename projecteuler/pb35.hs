isPrime :: Int -> Bool
isPrime n = isPrimeHelper n 2
            where isPrimeHelper n p
                    | n < p*p = True
                    | mod n p == 0 = False
                    | p == 2 = isPrimeHelper n 3
                    | otherwise = isPrimeHelper n (p+2)

rotate :: String -> String
rotate (x:xs) = xs ++ [x]

circularPrime :: Int -> Bool
circularPrime n = if any (\c -> c `elem` s) "02468" then False else circularPrimeHelper s (length s) 
                  where circularPrimeHelper s i
                          | i == 0 = True
                          | isPrime (read s) = circularPrimeHelper (rotate s) (i-1)
                          | otherwise = False
                        s = show n

main :: IO ()
main = print $ length $ [2] ++ [i | i <- [3..999999], circularPrime i]
