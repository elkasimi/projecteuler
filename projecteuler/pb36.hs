import Numeric (showIntAtBase)
import Data.Char (intToDigit)

palindrome :: Int -> Bool
palindrome n = (odd n) && (d == reverse d) && (b == reverse b)
               where d = show n
                     b = showIntAtBase 2 intToDigit n ""

main :: IO ()
main = print $ sum $ [i | i <- [1..999999], palindrome i]
