isPalindrome n = s == (reverse s)
  where s = (show n)

palindrome a b
 | b < 100 = 0
 | isPalindrome p = p
 | otherwise = palindrome a (b-1)
 where p = a*b

findPalindrome a m
  | a*a <= m = m
  | otherwise = findPalindrome (a-1) (max m (palindrome a a))

findLargestPalindrome a m
  | a*a <= m = m
  | otherwise = findLargestPalindrome (a-1) (max m (findPalindrome a 0))

main = do
  print $ findLargestPalindrome 999 0
