multiple n m
  | n == 1 = m
  | otherwise = multiple (n-1) (lcm m n)

main = do
  print$ multiple 20 1
