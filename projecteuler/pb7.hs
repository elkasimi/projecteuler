isPrime primes n
  | primes == [] = True
  | first ^ 2 > n = True
  | (n `mod` first) == 0 = False
  | otherwise = isPrime (tail primes) n
  where first = head primes

nextPrime primes current n
  | n == 0 = last primes
  | isPrime primes current = nextPrime (primes ++ [current]) (current + 1) (n - 1)
  | otherwise = nextPrime primes (current + 1) n

main = do
  n <- readLn
  print $ nextPrime [] 2 n
